#!/bin/bash

# Project-specific configurations
PROJECT_NAME="tempman"
COMMANDS_DIR="commands"
HELPERS_FILE="helpers.sh"
HANDLER_INI="handle_ini.sh"
HANDLER_CLI="handle_cli.sh"

# Fail on a single failed command in a pipeline
set -o pipefail

# Save global script args
ARGS="$@"

# Fail on error and undefined vars
set -eu

# Main loop evaluating sub commands
run() {
    local first_arg=${1:-}
    local cmd_dir="$(basedir)/${COMMANDS_DIR}"
    local command

    if [ -n "$first_arg" ] && [[ ${first_arg} != -* ]]; then
        command="$first_arg"
        if [ ! -f "$cmd_dir/$command" ]; then
            echo
            echo ">>>> Unknown command '$command'"
            echo
            display_help
            exit 1
        fi
    else
        command="help"
        echo "No command given"
        echo
    fi

    if [ "${command}" = "help" ] || $(hasflag --help -h); then
        display_help ${command:-}
        exit 0
    fi

    # Assign config file path
    local cfg=$(readopt -c --cfg)
    if [ ! -z "${cfg}" ] && [ -e "${cfg}" ]; then
      INI_FILE="$cfg"
    elif [ ! -z "${cfg}" ] && [ ! -e "${cfg}" ]; then
        echo "ERROR: '$cfg' does not exist."
        exit 1
    elif [ -z "${cfg}" ]; then
      INI_FILE=""
    fi

    source "$cmd_dir/$command"

    eval "${command}::run"
}

display_help() {
    local command=${1:-}
    local cmd_dir="$(basedir)/${COMMANDS_DIR}"

    if [ -z "${command}" ] || [ "$command" = "help" ]; then
        cat << EOT
Usage: $PROJECT_NAME <command> <opts>

${PROJECT_NAME} - a bash tool for template management by Cs137

Commands:
EOT
        for cmd in $(ls $cmd_dir); do
            if [ -f $cmd_dir/$cmd ]; then
                source $cmd_dir/$cmd
                printf "   %-15s  %s\n" $cmd "$($cmd::description)"
            fi
        done
        printf "   %-15s  %s\n" "help" "Print this help message and exit"
    else
        source $cmd_dir/$command
        cat <<EOT
$($command::description)

Usage: $PROJECT_NAME $command <opts>

EOT
        echo "Options for $command:"
        echo -e "$($command::usage)"
    fi

    cat <<EOT

Global Options:
  -c --cfg <path>   Path to $PROJECT_NAME configuration file (mandatory)
  -f --force        Perform actions without confirmation
  -v --verbose      Display detailed output
  -h --help         Print this help message and exit
EOT
}

# Directory where this script is located
basedir() {
    local script=${BASH_SOURCE[0]}

    # Resolve symbolic links
    if [ -L $script ]; then
        if readlink -f $script >/dev/null 2>&1; then
            script=$(readlink -f $script)
        elif readlink $script >/dev/null 2>&1; then
            script=$(readlink $script)
        elif realpath $script >/dev/null 2>&1; then
            script=$(realpath $script)
        else
            echo "ERROR: Cannot resolve symbolic link $script"
            exit 1
        fi
    fi

    local dir=$(dirname "$script")
    local full_dir=$(cd "${dir}" && pwd)
    echo ${full_dir}
}

# ===========================================================
# Startup ...

# Read in helpers
source "$(basedir)/$HELPERS_FILE"
source "$(basedir)/$HANDLER_INI"
source "$(basedir)/$HANDLER_CLI"

if $(hasflag --verbose -v); then
    export PS4='+($(basename ${BASH_SOURCE[0]}):${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
    set -x
fi

run $ARGS
