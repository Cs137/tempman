cli_selection() {
  local prompt="$1"
  shift
  local options=("$@")

  PS3="$prompt "
  local choice

  select choice in "${options[@]}"; do
    if [ -n "$choice" ]; then
      break
    else
      echo "Invalid selection. Please try again."
    fi
  done

  echo "$choice"
}

cli_confirmation() {
  local msg=$1

  if ! $(hasflag --force -f); then
    read -p "$msg? (y/n): " confirmation
    if [ "$confirmation" != "y" ]; then
      return 1
    fi
  fi
  return 0
}
