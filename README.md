# Template Management

A straightforward tool for organising and managing templates.

I maintain separate template repositories for distinct project types. This separation
avoids mingling personal and professional templates, ensuring a certain modularity.

Each template repository categorises templates into folders within a `templates`
directory. Templates are organised in categories, and the category is the name of
a directory inside the `templates` directory. To aggregate multiple template
repositories, I employ a `Makefile`, a part of each repository providing `link`
and `unlink` targets. You can find an example repository [here](https://codeberg.org/Cs137/templates).

Linking enables maintaining templates in different repositories while having a
choice from all known templates in a single directory. I refer to this directory
as the "templates root directory." The path to this directory is defined in the
[`TEMPLATE_ROOT_DIR`](#environment-variables) environment variable in each
relevant environment.

This package provides commands to add, update, and remove template repositories
on a local machine. It takes care of (un)linking using the repository's `Makefile`
and offers a command to select a template from the "templates root directory" for
a new project. Upon choosing, the template files are copied into a new project's
directory, and post-creation procedures follow. This includes renaming `.gitignore`,
deleting `.gitkeep` files, adding the project title to `README.md`, and optionally
initialising the new project's directory as a Git repository.


## Usage

To see a list of available commands and their descriptions, run:

```bash
./tempman --help
```

To get detailed help for a specific command, run:

```bash
./YourProjectName [COMMAND] --help
```

### Quick start

1. [Add a template repository locally and link its templates to the template root directory.](#add-external-template-repository)
2. [Use a template from the template root directory for a new project.](#use)


### Commands

#### `repo`

The `repo` command allows to manage local copies of external repositories,
structured like the one mentioned in [*Usage*](#usage).

##### Add external template repository

The example underneath adds a local copy of the aforementioned repository and
updates the `ini` file. Since no `--name` is provided, the directory name of the
local path will be used as repository (section) name in the `ini` file.

Moreover, the templates from the repository are linked to the template root
directory using the repository's `Makefile`. The latter relies on the definition
of the templates root directory as environment variable, read the [corresponding section](#environment-variables) for more information.

```bash
./tempman repo -c=repos.ini -p=./test_templates -r=https://codeberg.org/Cs137/templates.git -a
```

##### Update external template repository

To update a certain template repository, provide either a path to the `ini` file,
or a local path to a template repository. If no path is declared, all repositories
listed in the file are taken into account. Alternatively, you can also provide a
configuration file and a name in order to update a specific repository.

```bash
./tempman repo -c=repos.ini -u
```

##### Remove external template repository

The data of a repository, including its `ini` file entry can be removed by
specifying the repository's name, as follows:

```bash
./tempman repo -c=repos.ini -n=test_templates -d
```

#### `use`

The `use` command offers a basic utility to create a new project based on one
of the existing templates.

It will create a new folder for the project and copy the content of the template
for the defined type from its folder into the new project's folder. Moreover, the
`gitignore` file is renamed, the `.gitkeep` files are removed, Git is (optionally)
initialised and the project's title is added to the project's README file. Example:

```bash
./tempman use -n=test_project -g
```


## Configuration

### `ini` file

A configuration file is used to store added repositories and allows the removal
and update of all template repositories at once. The file must be a plain text
file formatted like an `ini` file, like shown in the example underneath.

```bash
[repository1]
/path/to/repo1

[repository2]
/path/to/repo2
```

If you add repositories via `tempman ... -a`, the file will get updated with the
new repository name and its path. Entries are also removed it `tempman ... -d`
is used for the removal of a repository.

### Environment Variables

- `TEMPLATE_ROOT_DIR`, is used to define a template directory. You can set this
  variable to customise the location. If `TEMPLATE_ROOT_DIR` is not set, the
  default value `~/templates` will be used.


## Notes

- The Makefiles of the template repositories use [GNU Stow](https://www.gnu.org/software/stow/)
  to manage symbolic links, ensure that it is installed on your system.


## Contributing
See [`CONTRIBUTING.md`](CONTRIBUTING.md) for guidelines.


## License
This project is licensed under the [MIT License](`LICENSE`).


## Acknowledgments

- This project's structure is inspired by the talk [*"Shell Ninja: Mastering the Art of Shell Scripting"*](https://youtu.be/1mt2-LbKuvY), special thanks to Dr. Roland Huß for sharing those insights ([*shell-ninja* repository](https://github.com/ro14nd-talks/shell-ninja)).
