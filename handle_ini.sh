ini_existing() {
  [ -e "${INI_FILE}" ]
}

ini_not_empty() {
  [ -z "${INI_FILE}" ]
}

get_ext_repo_names() {
  # returns array of section names
  local repo_names=($(read_from_ini $INI_FILE --list))
  echo "${repo_names[@]}"
}

get_ext_repo_path() {
  # returns local path for provided repository name
  local repo_name="$1"

  local repo_path=$(read_from_ini $INI_FILE $repo_name path_local)
  echo "$repo_path"
}

add_ext_repo_to_ini() {
  # adds an external repository as section to the ini file
  local repo_name="$1"
  local repo_path="$2"

  local add_repo=$(add_to_ini $INI_FILE $repo_name path_local $repo_path)
  check_error "$add_repo"
}

rm_ext_repo_from_ini() {
  # remove an external repository section from the ini file
  local repo_name="$1"

  local rm_repo=$(remove_from_ini $INI_FILE $repo_name)
  check_error "$rm_repo"
}

add_to_ini() {
  local inifile="$1"
  local section="$2"
  local key="$3"
  local value="$4"

  # Check if the section already exists
  if grep -q "\[${section}\]" "$inifile"; then
    echo "ERROR: ini handler - Section '$section' already exists in '$inifile'."
  else
    # If the section doesn't exist, append it along with the key-value pair
    echo -e "\n[${section}]" >> "$inifile"
    echo "${key} = ${value}" >> "$inifile"
    echo "Added '$key' under section '$section' in '$inifile'."
  fi
}

remove_from_ini() {
  local inifile="$1"
  local section="$2"

  # Remove the entire section from the INI file
  sed -i "/^\[${section}\]/,/^$/d" "$inifile"
  # clean empty lines
  local cleaned=$(cat "$INI_FILE" | awk '!NF {if (++n == 1) print ""; next} {n = 0; print}')
  echo -e "$cleaned" >"$INI_FILE"
  echo "Removed section '$section' from '$inifile'."
}

read_from_ini() {
    if [[ $# -lt 2 || ! -f $1 ]]; then
        echo "usage: read_ini <file> [--list|<section> [key]]"
        return 1
    fi
    local inifile=$1

    if [ "$2" == "--list" ]; then
        for section in $(cat $inifile | grep "^\\s*\[" | sed -e "s#\[##g" | sed -e "s#\]##g"); do
            echo $section
        done
        return 0
    fi

    local section=$2
    local key
    [ $# -eq 3 ] && key=$3

    # This awk line turns ini sections => [section-name]key=value
    local lines=$(awk '/\[/{prefix=$0; next} $1{print prefix $0}' $inifile)
    lines=$(echo "$lines" | sed -e 's/[[:blank:]]*=[[:blank:]]*/=/g')
    while read -r line ; do
        if [[ "$line" = \[$section\]* ]]; then
            local keyval=$(echo "$line" | sed -e "s/^\[$section\]//")
            if [[ -z "$key" ]]; then
                echo $keyval
            else
                if [[ "$keyval" = $key=* ]]; then
                    echo $(echo $keyval | sed -e "s/^$key=//")
                fi
            fi
        fi
    done <<<"$lines"
}
